package mem

/*
#include <mach/mach_host.h>
*/
import "C"
import (
	"fmt"
	"syscall"
	"unsafe"
)

func (o *Mem) Update() error {
	var all, used, free uint64
	var vmstat C.vm_statistics_data_t
	var count C.mach_msg_type_number_t = C.HOST_VM_INFO_COUNT

	val, err := syscall.Sysctl("hw.memsize")
	if err != nil {
		return err
	}
	buf := []byte(val)
	all = *(*uint64)(unsafe.Pointer(&buf[0]))

	status := C.host_statistics(
		C.host_t(C.mach_host_self()),
		C.HOST_VM_INFO,
		C.host_info_t(unsafe.Pointer(&vmstat)),
		&count)
	if status != C.KERN_SUCCESS {
		return fmt.Errorf("host_statistics error=%d", status)
	}

	free = uint64(vmstat.free_count) << 12
	used = all - free

	o.All = float64(all) / float64(GB)
	o.Used = float64(used) / float64(GB)
	o.Free = float64(free) / float64(GB)
	return nil
}
