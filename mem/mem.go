package mem

const (
	B  = 1
	KB = 1024 * B
	MB = 1024 * KB
	GB = 1024 * MB
)

type Mem struct {
	All       float64
	Used      float64
	Free      float64
	Available float64
}
