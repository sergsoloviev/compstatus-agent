package main

import (
	"bitbucket.org/sergsoloviev/compstatus-agent/disk"
	"bitbucket.org/sergsoloviev/compstatus-agent/mem"
	"github.com/k0kubun/pp"
)

func main() {

	fs := disk.FileSystemStatus{MountPoint: "/"}
	fs.Update()
	pp.Println(fs)

	memory := mem.Mem{}
	memory.Update()
	pp.Println(memory)

}
